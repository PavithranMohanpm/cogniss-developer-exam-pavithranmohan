const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")
const getandWrite_Acronym = function() {
  var acronym = randomstring.generate(3).toUpperCase();
  got(inputURL+acronym).then(response => {
    console.log("got data for acronym", acronym);
    console.log("add returned data to definitions array");
    output.definitions.push(response.body);
    console.log("Checing output", output.definitions)

    if (output.definitions.length < 10) {
      console.log("calling looping function again");
      getandWrite_Acronym();
    }else{
        //Changed the code so that the JSON writing aspect of the code is performed inside the function
        //Provided the number of definitions is 10.
    console.log("saving output file formatted with 2 space indenting");
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
    console.log("All done!");
});   
    }

  
  }).catch(err => {
    console.log(err)
  })
}
console.log("calling looping function");
getandWrite_Acronym();

