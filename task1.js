const fs = require("fs");
const jsonfile = require("jsonfile");
const rs = require("randomstring");
inputfile = 'input2.json';
outputfile = 'output2.json';
console.log('Accessing input2.json');
//Simple function to reverse the string.
function stringreverse(str){
    return str.split('').reverse().join('');
}

//Read the file, and perform the operation in one go.

fs.readFile(inputfile, 'utf8', (err, jsonString) => {
    if (err) {
        console.log("No file found, check status of file:", err);
        return
    }try {
        const names_json = JSON.parse(jsonString);
        const names = names_json.names;
        const fakeEmails = [];
        for(i = 0; i < names.length;i++){
            const random = rs.generate({
                length: 5,
                charset: 'alphabetic'
              });
            const fakeEmail = stringreverse(names[i]).toLowerCase() + random.toLowerCase() + '@gmail.com'
            fakeEmails.push(fakeEmail);
        }
        const ouputJson = {
            emails : fakeEmails
        }
        jsonfile.writeFile(outputfile, ouputJson, {spaces: 2}, function(err) {
            console.log("All done!");
          });
       
} catch(err) {
    console.log('Error parsing JSON:', err);
}
})


